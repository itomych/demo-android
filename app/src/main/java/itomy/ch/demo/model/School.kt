package itomy.ch.demo.model

/**
 * Created by Yegor on 11/10/17.
 */
data class School(val id: String, val name: String, val links: Links) {
}