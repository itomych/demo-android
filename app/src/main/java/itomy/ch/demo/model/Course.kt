package itomy.ch.demo.model

/**
 * Created by Yegor on 11/10/17.
 */
data class Course(val id: String,
                  val name: String,
                  val thumbnail: String,
                  val subject: String)